export "$(cat .env | xargs)"

echo "Which image do you want to build and push to Gitlab Artifact Registry?"
read -r foldername

docker login registry.gitlab.com -p "$GITLAB_TOKEN" -u "$GITLAB_USERNAME"

docker build -t registry.gitlab.com/emilioesteve99/back-nestjs-boilerplate/"$foldername":latest ./"$foldername"

docker push registry.gitlab.com/emilioesteve99/back-nestjs-boilerplate/"$foldername":latest
