import { UserFindQueryFixtures } from './UserFindQueryFixtures';
import { UserPaginateFindQuery } from '../../../domain/query/UserPaginateFindQuery';

export class UserPaginateFindQueryFixtures {
  public static get any(): UserPaginateFindQuery {
    const userPaginateFindQuery: UserPaginateFindQuery = new UserPaginateFindQuery(UserFindQueryFixtures.any, {
      limit: 10,
      page: 1,
    });

    return userPaginateFindQuery;
  }
}
