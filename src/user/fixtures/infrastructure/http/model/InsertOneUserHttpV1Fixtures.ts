import { InsertOneUserHttpV1 } from '../../../../infrastructure/http/model/InsertOneUserHttpV1';
import { UserFixtures } from '../../../domain/model/UserFixtures';

export class InsertOneUserHttpV1Fixtures {
  public static get any(): InsertOneUserHttpV1 {
    const insertOneUserHttpV1: InsertOneUserHttpV1 = new InsertOneUserHttpV1();

    insertOneUserHttpV1.email = UserFixtures.any.email;
    insertOneUserHttpV1.name = UserFixtures.any.name;
    insertOneUserHttpV1.password = UserFixtures.any.password;

    return insertOneUserHttpV1;
  }
}
