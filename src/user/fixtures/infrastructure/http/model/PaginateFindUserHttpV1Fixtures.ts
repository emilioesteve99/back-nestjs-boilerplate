import { PaginateFindUserHttpV1 } from '../../../../infrastructure/http/model/PaginateFindUserHttpV1';

export class PaginateFindUserHttpV1Fixtures {
  public static get any(): PaginateFindUserHttpV1 {
    const paginateFindUserHttpV1: PaginateFindUserHttpV1 = new PaginateFindUserHttpV1();

    return paginateFindUserHttpV1;
  }
}
