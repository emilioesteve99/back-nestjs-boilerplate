import { QueryBus } from '@nestjs/cqrs';
import { expect, Mocked } from 'vitest';

import { BaseUserController } from './BaseUserController';
import { User } from '../../../domain/model/User';
import { UserFixtures } from '../../../fixtures/domain/model/UserFixtures';

class TestBaseUserController extends BaseUserController {
  public override fakeUserPassword(input: User): User {
    return super.fakeUserPassword(input);
  }

  public override fakeUsersPassword(input: User[]): User[] {
    return super.fakeUsersPassword(input);
  }
}

describe(BaseUserController.name, () => {
  let queryBusMock: Mocked<QueryBus>;

  let testBaseUserController: TestBaseUserController;

  beforeAll(() => {
    queryBusMock = {
      execute: vi.fn(),
    } as Partial<Mocked<QueryBus>> as Mocked<QueryBus>;

    testBaseUserController = new TestBaseUserController(queryBusMock);
  });

  describe('.fakeUserPassword()', () => {
    describe('when called', () => {
      let userFixture: User;
      let result: unknown;

      beforeAll(() => {
        userFixture = UserFixtures.any;

        result = testBaseUserController.fakeUserPassword(userFixture);
      });

      afterAll(() => {
        vi.clearAllMocks();
      });

      it('should return a User with a fake password', () => {
        expect(result).toStrictEqual({
          ...userFixture,
          password: 'fake-user-password-example',
        });
      });
    });
  });

  describe('.fakeUsersPassword()', () => {
    describe('when called', () => {
      let userFixtures: User[];
      let result: unknown;

      beforeAll(() => {
        userFixtures = [UserFixtures.any];

        result = testBaseUserController.fakeUsersPassword(userFixtures);
      });

      afterAll(() => {
        vi.clearAllMocks();
      });

      it('should return a User[] with fake passwords', () => {
        expect(result).toStrictEqual([
          {
            ...userFixtures[0],
            password: 'fake-user-password-example',
          },
        ]);
      });
    });
  });
});
