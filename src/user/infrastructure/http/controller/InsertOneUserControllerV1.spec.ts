import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { expect, Mocked } from 'vitest';

import { InsertOneUserControllerV1 } from './InsertOneUserControllerV1';
import { UserInsertOneCommand } from '../../../domain/command/UserInsertOneCommand';
import { User } from '../../../domain/model/User';
import { UserInsertOneCommandFixtures } from '../../../fixtures/domain/command/UserInsertOneCommandFixtures';
import { UserFixtures } from '../../../fixtures/domain/model/UserFixtures';
import { InsertOneUserHttpV1Fixtures } from '../../../fixtures/infrastructure/http/model/InsertOneUserHttpV1Fixtures';
import { InsertOneUserHttpV1 } from '../model/InsertOneUserHttpV1';

describe(InsertOneUserControllerV1.name, () => {
  let commandBusMock: Mocked<CommandBus>;
  let queryBusBusMock: Mocked<QueryBus>;

  let insertOneUserControllerV1: InsertOneUserControllerV1;

  beforeAll(() => {
    commandBusMock = {
      execute: vi.fn(),
    } as Partial<Mocked<CommandBus>> as Mocked<CommandBus>;
    queryBusBusMock = {
      execute: vi.fn(),
    } as Partial<Mocked<QueryBus>> as Mocked<QueryBus>;

    insertOneUserControllerV1 = new InsertOneUserControllerV1(commandBusMock, queryBusBusMock);
  });

  describe('.insertOne()', () => {
    describe('when called', () => {
      let insertOneUserHttpV1Fixture: InsertOneUserHttpV1;
      let userInsertOneCommandFixture: UserInsertOneCommand;
      let userFixture: User;
      let result: unknown;

      beforeAll(async () => {
        insertOneUserHttpV1Fixture = InsertOneUserHttpV1Fixtures.any;
        userInsertOneCommandFixture = UserInsertOneCommandFixtures.any;
        userFixture = UserFixtures.any;

        commandBusMock.execute.mockResolvedValueOnce(userFixture);

        result = await insertOneUserControllerV1.insertOne(insertOneUserHttpV1Fixture);
      });

      afterAll(() => {
        vi.clearAllMocks();
      });

      it('should call commandBus.execute()', () => {
        expect(commandBusMock.execute).toHaveBeenCalledOnce();
        expect(commandBusMock.execute).toHaveBeenCalledWith(userInsertOneCommandFixture);
      });

      it('should return a User', () => {
        expect(result).toStrictEqual({
          ...userFixture,
          password: 'fake-user-password-example',
        });
      });
    });
  });
});
