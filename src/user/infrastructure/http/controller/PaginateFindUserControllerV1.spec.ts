import { QueryBus } from '@nestjs/cqrs';
import { Mocked } from 'vitest';

import { PaginateFindUserControllerV1 } from './PaginateFindUserControllerV1';
import { Pagination } from '../../../../common/domain/model/Pagination';
import { User } from '../../../domain/model/User';
import { UserPaginateFindQuery } from '../../../domain/query/UserPaginateFindQuery';
import { UserFixtures } from '../../../fixtures/domain/model/UserFixtures';
import { UserPaginateFindQueryFixtures } from '../../../fixtures/domain/query/UserPaginateFindQueryFixtures';
import { PaginateFindUserHttpV1Fixtures } from '../../../fixtures/infrastructure/http/model/PaginateFindUserHttpV1Fixtures';
import { PaginateFindUserHttpV1 } from '../model/PaginateFindUserHttpV1';

describe(PaginateFindUserControllerV1.name, () => {
  let queryBusMock: Mocked<QueryBus>;

  let paginateFindUserControllerV1: PaginateFindUserControllerV1;

  beforeAll(() => {
    queryBusMock = {
      execute: vi.fn(),
    } as Partial<Mocked<QueryBus>> as Mocked<QueryBus>;

    paginateFindUserControllerV1 = new PaginateFindUserControllerV1(queryBusMock);
  });

  describe('.paginateFind()', () => {
    describe('when called', () => {
      let paginateFindUserHttpV1Fixture: PaginateFindUserHttpV1;
      let userPaginateFindQueryFixture: UserPaginateFindQuery;
      let paginatedUsersFixture: Pagination<User>;
      let result: unknown;

      beforeAll(async () => {
        paginateFindUserHttpV1Fixture = PaginateFindUserHttpV1Fixtures.any;
        userPaginateFindQueryFixture = UserPaginateFindQueryFixtures.any;
        paginatedUsersFixture = {
          items: [UserFixtures.any],
          meta: {
            currentPage: 1,
            itemCount: 1,
            itemsPerPage: 1,
            totalItems: 1,
            totalPages: 1,
          },
        };

        queryBusMock.execute.mockResolvedValueOnce(paginatedUsersFixture);

        result = await paginateFindUserControllerV1.paginateFind(paginateFindUserHttpV1Fixture);
      });

      afterAll(() => {
        vi.clearAllMocks();
      });

      it('should call queryBus.execute()', () => {
        expect(queryBusMock.execute).toHaveBeenCalledOnce();
        expect(queryBusMock.execute).toHaveBeenCalledWith(userPaginateFindQueryFixture);
      });

      it('should return a Pagination<User>', () => {
        expect(result).toStrictEqual({
          ...paginatedUsersFixture,
          items: paginatedUsersFixture.items.map((user: User) => ({
            ...user,
            password: 'fake-user-password-example',
          })),
        });
      });
    });
  });
});
